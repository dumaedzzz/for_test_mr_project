const express = require('express');
const cors = require('cors');

const server = express();
const port = 9000;
server.use(cors());

server.get('/', (req, res) => {
    res.send('Hello team')
})

server.listen(port, () => {
  console.log(`Server is started on ${port} port !`);
})
